// entry point, runs under the assumption that a task scheduler calls the script

import hospital1Controller from './controllers/hospital1Controller'
import hospital2Controller from './controllers/hospital2Controller'
import dotenv from 'dotenv'
import DataAccess from './lodaers/sequlize'
dotenv.config()

const runServer = async () => {
	try {
		await DataAccess.initConnection()
		await hospital1Controller()
		await hospital2Controller()
	} catch (error) {
		console.error('proccess run failed')
	}
	// } finally {
	// 	await DataAccess.closeConnection()
	// }
}

runServer()
