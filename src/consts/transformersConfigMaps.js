export const hospital1Config = {
	patient: {
		'PatientID':'patientID',  
		'MRN':'MRN',
		'PatientDOB':'patientDOB', 
		'IsDeceased':'isDeceased',
		'DOD_TS':'DOD_TS',    
		'LastName':'lastName',
		'FirstName':'firstName', 
		'Gender':'gender',
		'Sex':'sex',       
		'Address':'address',
		'City':'city',      
		'State':'state',
		'ZipCode':'zipCode',   
		'LastModifiedDate':'lastModifiedDate'
	},
	treatment: {
		'TreatmentID': 'treatmentID',
		'PatientID': 'patientID',
		'TreatmentLine': 'treatmentLine',
		'StartDate': 'startDate',
		'EndDate': 'endDate',
		'Active': 'status',
		'DisplayName': 'displayName',
		'Diagnoses': 'diagnoses',
		'CyclesXDays': 'numberOfCycles'
	}
}

export const hospital2Config = {
	patient: {
		'PatientId': 'patientID',  
		'MRN':'MRN',
		'PatientDOB':'patientDOB', 
		'IsPatientDeceased': 'isDeceased',
		'DeathDate': 'DOD_TS',    
		'LastName':'lastName',
		'FirstName':'firstName', 
		'Gender':'gender',
		'Sex':'sex',       
		'AddressLine': 'address',
		'AddressCity': 'city',      
		'AddressState': 'state',
		'AddressZipCode': 'zipCode',   
		'LastModifiedDate':'lastModifiedDate'
	},
	treatment: {
		'TreatmentId': 'treatmentID',
		'PatientId': 'patientID',
		'ProtocolID': 'treatmentLine',
		'StartDate': 'startDate',
		'EndDate': 'endDate',
		'Status': 'status',
		'AssociatedDiagnoses': 'diagnoses',
		'DisplayName': 'displayName',
		'NumberOfCycles': 'numberOfCycles'
	}
}