// generic transformer for patient entity

import _ from 'lodash'
import { deceasedTrueOptions } from '../consts/consts'
 

const patientTransformer = (patients, configMap) => {
	return patients.map(patient => {
		const transformed = _.mapKeys(patient, (_, key) => {
			//csv library fix
			return configMap[key] ? configMap[key] : configMap[Object.keys(configMap)[0]]
		})
		return {
			...transformed,
			isDeceased: deceasedTrueOptions.includes(transformed.isDeceased)
		}
	})
}

export default patientTransformer