// generic transformer for patient entity

import _ from 'lodash'
 
const treatmentTransformer = (treatments, configMap) => {
	return treatments.map(treatment => {
		const transformed = _.mapKeys(treatment, (_, key) => {
			//csv library fix
			return configMap[key] ? configMap[key] : configMap[Object.keys(configMap)[0]]
		})
		return {
			...transformed,
			numberOfCycles: transformed?.numberOfCycles?.indexOf('X')
				? Number(transformed.numberOfCycles.slice(0, transformed.numberOfCycles.indexOf('X')))
				: Number(transformed.numberOfCycles),
			endDate: transformed.endDate &&  transformed.endDate !== 'NULL' || null
		}
	})
}

export default treatmentTransformer