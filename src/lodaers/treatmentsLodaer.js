// the loader for treatments
import DataAccess from './sequlize'

const treatmentsLoader = async (treatments) => {
	try{
		await DataAccess.sequelize.models.treatment.bulkCreate(treatments)
	} catch(error) {
		console.error(error)
	}
}

export default treatmentsLoader