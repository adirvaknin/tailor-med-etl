import { DataTypes } from 'sequelize'

const patientModel = async (sequelize) => {
	const patient = sequelize.define('patient',{
		'patientID':{
			type: DataTypes.INTEGER,
			primaryKey: true
		},  
		'MRN':{
			type: DataTypes.DATE,
		},
		'patientDOB':{
			type: DataTypes.STRING,
		}, 
		'isDeceased':{
			type: DataTypes.BOOLEAN,
		},
		'DOD_TS':{
			type: DataTypes.STRING,
		},    
		'lastName':{
			type: DataTypes.STRING,
		},
		'firstName':{
			type: DataTypes.STRING,
		}, 
		'gender':{
			type: DataTypes.STRING,
		},
		'sex':{
			type: DataTypes.STRING,
		},       
		'address':{
			type: DataTypes.STRING,
		},
		'city':{
			type: DataTypes.STRING,
		},      
		'state':{
			type: DataTypes.STRING,
		},
		'zipCode':{
			type: DataTypes.STRING,
		},   
		'lastModifiedDate':{
			type: DataTypes.DATE,
			defaultValue: DataTypes.NOW
		}
	})
	//used for demo purposes only, usualy will be replaced with sqitch / liquibase etc.  
	await patient.sync()
}

export default patientModel