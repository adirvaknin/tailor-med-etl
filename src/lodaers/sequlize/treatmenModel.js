import { DataTypes } from 'sequelize'

const initTreatmenModel = async (sequelize) => {
	const treatment = sequelize.define('treatment',{
		'treatmentID':{
			type: DataTypes.STRING,
			primaryKey: true
		},
		'patientID': {
			type: DataTypes.INTEGER,
		},
		'treatmentLine': {
			type: DataTypes.STRING,
		},
		'startDate': {
			type: DataTypes.STRING,
		},
		'endDate': {
			type: DataTypes.STRING
		},
		'status': {
			type: DataTypes.STRING,
		},
		'displayName': {
			type: DataTypes.STRING,
		},
		'diagnoses': {
			type: DataTypes.STRING,
		},
		'numberOfCycles': {
			type: DataTypes.INTEGER,
		}
	})
	//used for demo purposes only, usualy will be replaced with sqitch / liquibase etc.  
	await treatment.sync()
}

export default initTreatmenModel