/* eslint-disable no-undef */
import cls from 'cls-hooked'
import { Sequelize } from 'sequelize'
import patientModel from './patientModel'
import initTreatmenModel from './treatmenModel'
const transactionNameSpace = cls.createNamespace('transactionNameSpace')
Sequelize.useCLS(transactionNameSpace)

class DataAccess {
	static sequelize
	static async initConnection () {
		try {
			this.sequelize = new Sequelize(process.env.CONNECTION_STRING)
			await this.sequelize.authenticate()
			await patientModel(this.sequelize)
			await initTreatmenModel(this.sequelize)
			console.log('Connection has been established successfully.')
		} catch (error) {
			console.error('Unable to connect to the database:', error)
		}
	}

	static async closeConnection() {
		try {
			await this.sequelize.close()
			console.log('Connection has been closed successfully.')
		} catch (error) {
			console.error('Unable to close connection to the database:', error)
		}
	}

	static async executeWithTransaction(callback) {
		try {
			return await this.sequelize.transaction(async () => {
				await callback()
			})
		} catch (error) {
			console.error(error)
		}
	}
}

export default DataAccess