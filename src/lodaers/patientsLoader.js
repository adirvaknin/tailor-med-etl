// the lodaer for patients

import DataAccess from './sequlize'

const patientsLoader = async (patients) => {
	try{
		await DataAccess.sequelize.models.patient.bulkCreate(patients)
	} catch(error) {
		console.error(error)
	}
}

export default patientsLoader