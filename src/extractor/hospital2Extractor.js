// the extractor for hospital 2
import csv from 'csv-parser'
import fs from 'fs'

class hospital2Extractor {
	static batches = {
		patients: [],
		treatments: [],
	}
	// eslint-disable-next-line no-undef
	static batchSize = process.env.BATCH_SIZE || 1

	static checkCallBackBeforeActivate = (callback, row, rowType) => {
		this.batches[rowType] = [...this.batches[rowType], row]
		if(this.batches[rowType].length >= this.batchSize){
			callback({
				patients: [],
				treatments: [],
				[rowType]: this.batches[rowType]
			})
			this.batches[rowType] = []
		}
	}
	static  extractHospital2Data = async (onDataCallBack) => {
		fs.createReadStream('./src/extractor/mockData/hospital_2_Patient.csv')
			.on('error', (error) => {
				console.error('error with hospital2 patients', error)
			})

			.pipe(csv())
			.on('data', (row) => {
				this.checkCallBackBeforeActivate(onDataCallBack, row, 'patients')
			})

			.on('end', () => {
				console.log('ended reading hospital2 patients')
			})

		fs.createReadStream('./src/extractor/mockData/hospital_2_Treatment.csv')
			.on('error', () => {
				console.error('error with hospital2 treatment')
			})

			.pipe(csv())
			.on('data', async (row) => {
				await this.checkCallBackBeforeActivate(onDataCallBack, row, 'treatments')
			})

			.on('end', () => {
				console.log('ended reading hospital2 patients')
			})
	}
}
export default hospital2Extractor