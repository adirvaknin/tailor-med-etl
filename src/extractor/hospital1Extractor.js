// the extractor for hospital 1
import csv from 'csv-parser'
import fs from 'fs'
class hospital1Extractor {
	static batches = {
		patients: [],
		treatments: [],
	}
	// eslint-disable-next-line no-undef
	static batchSize = process.env.BATCH_SIZE || 1

	static checkCallBackBeforeActivate = (callback, row, rowType) => {
		this.batches[rowType] = [...this.batches[rowType], row]
		if(this.batches[rowType].length >= this.batchSize){
			callback({
				patients: [],
				treatments: [],
				[rowType]: this.batches[rowType]
			})
			this.batches[rowType] = []
		}
	}
	static extractHospital1Data = async (onDataCallBack) => {
		fs.createReadStream('./src/extractor/mockData/hospital_1_Patient.csv')
			.on('error', (error) => {
				console.error('error with hospital1 patients', error)
			})

			.pipe(csv())
			.on('data', (row) => {
				this.checkCallBackBeforeActivate(onDataCallBack, row, 'patients')
			})

			.on('end', () => {
				console.log('ended reading hospital1 patients')
			})

		fs.createReadStream('./src/extractor/mockData/hospital_1_Treatment.csv')
			.on('error', () => {
				console.error('error with hospital1 treatment')
			})

			.pipe(csv())
			.on('data', (row) => {
				this.checkCallBackBeforeActivate(onDataCallBack, row, 'treatments')
			})

			.on('end', () => {
				console.log('ended reading hospital1 patients')
			})
	}
}
export default hospital1Extractor