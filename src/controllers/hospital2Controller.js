// the extractor for hospital 2
import { hospital2Config } from '../consts/transformersConfigMaps'
import hospital2Extractor from '../extractor/hospital2Extractor'
import patientsLoader from '../lodaers/patientsLoader'
import treatmentsLoader from '../lodaers/treatmentsLodaer'
import patientTransformer from '../transformers/patientTransformer'
import treatmentTransformer from '../transformers/treatmentTransformer'

const hospital2Controller = async () => {
	try {
		//EXTRACT
		await hospital2Extractor.extractHospital2Data(async (data) => {

			//TRANSFORM
			const transformePatients = patientTransformer(data.patients, hospital2Config.patient)
			const transformedTreatments = treatmentTransformer(data.treatments, hospital2Config.treatment)

			//LOAD
			await patientsLoader(transformePatients)
			await treatmentsLoader(transformedTreatments)
		})
	}catch(err) {
		console.error(err)
	}
}

export default hospital2Controller