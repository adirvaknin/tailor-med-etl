// the loader for hospital 1

import { hospital1Config } from '../consts/transformersConfigMaps'
import hospital1Extractor from '../extractor/hospital1Extractor'
import patientsLoader from '../lodaers/patientsLoader'
import treatmentsLoader from '../lodaers/treatmentsLodaer'
import patientTransformer from '../transformers/patientTransformer'
import treatmentTransformer from '../transformers/treatmentTransformer'

const hospital1Controller = async () => {
	try{
		//EXTRACT
		await hospital1Extractor.extractHospital1Data(async (data) =>{

			//TRANSFORM
			const transformePatients = patientTransformer(data.patients, hospital1Config.patient)
			const transformedTreatments = treatmentTransformer(data.treatments, hospital1Config.treatment)

			//LOAD
			await patientsLoader(transformePatients)
			await treatmentsLoader(transformedTreatments)
		})
	}catch(err) {
		console.error(err)
	}
}

export default hospital1Controller